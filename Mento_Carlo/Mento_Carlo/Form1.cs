﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mento_Carlo
{
    public partial class Form1 : Form
    {
        //S,K,T,r,sigma,N,I
        public double S { get; set; }
        public double K { get; set; }
        public double T { get; set; }
        public double r { get; set; }
        public double sigma { get; set; }
        public Int32 N { get; set; }
        public Int32 I { get; set; }
        public double[,] randomNumbers;
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.S = Convert.ToDouble(this.textBox1.Text);
            this.K = Convert.ToDouble(this.textBox2.Text);
            this.T = Convert.ToDouble(this.textBox3.Text);
            this.r = Convert.ToDouble(this.textBox4.Text);
            this.sigma = Convert.ToDouble(this.textBox5.Text);
            this.N = Convert.ToInt32(this.textBox6.Text);
            this.I = Convert.ToInt32(this.textBox7.Text);


            
            Simulator simulator = new Simulator(N,I);

            GaussianRNG g = new GaussianRNG();
            this.randomNumbers = g.RandomMatrix(N, I);
            double[] result = simulator.SimulatedPrice(S, K, T, r, sigma, N, I);
            double[] result2 = simulator.Greeks(S, K, T, r, sigma, N, I);

            string s = "the price and Standard Error of the European Call Option are " +result[0]+"and"+ result[2]
                + "the Greek Values of the European Call Option" + "the Delta:"+ result2[0] + "the Gamma:" + result2[2] + "the Vega:" + result2[4] + "the Theta:" + result2[6] + "the Rho:" + result2[8]
                + "the price and Standard Error of the European Put Error is " + result[1] + "and"+result[3]
                + "the Greek Values of the European Put Option " + "the Delta:" + result2[1] + "the Gamma:"+result2[3] + "the Vega:"+result2[5] + "the Theta:"+result2[7] + "the Rho:" + result2[9];


            MessageBox.Show(s);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            checksetentry((TextBox)sender, "S", false);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            checksetentry((TextBox)sender, "K", false);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.T = Convert.ToDouble(textBox3.Text);
            }
            catch(FormatException except)
            {
                MessageBox.Show(except.Message);
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            checksetentry((TextBox)sender, "r", true,true);
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            checksetentry((TextBox)sender, "sigma", true);
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.N = Convert.ToInt32(textBox6.Text);
            }
            catch (FormatException except)
            {
                MessageBox.Show(except.Message);
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.I = Convert.ToInt32(textBox7.Text);
            }
            catch(FormatException except)
            {
                MessageBox.Show(except.Message);
            }
        }
        private void checksetentry(TextBox tb, string PropertyName, bool CanBeZero, bool CanBeNegative = false)
        {
            double value = 0;
            try
            {
                value = Convert.ToDouble(tb.Text);
            }
            catch (FormatException except)
            {
                MessageBox.Show(except.Message);
            }

            if ((value > 0 || CanBeNegative) || (value == 0 && CanBeZero))
            {
                Type t = this.GetType();
                t.GetProperty(PropertyName).SetValue(this, value, null);
            }
            else
            {
                MessageBox.Show("Not a valid entry");
            }

        }

       
    }
}
