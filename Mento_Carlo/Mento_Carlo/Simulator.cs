﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mento_Carlo
{
    public class Simulator
    {
        public double[,] randomNumbers;
        public Simulator(Int32 N, Int32 I)
        {
            GaussianRNG g = new GaussianRNG();
            this.randomNumbers = g.RandomMatrix(N, I);
        }
        public double[] SimulatedPrice(double S, double K, double T, double r, double sigma, Int32 N, Int32 I)
        {
            //S:spotprice; K:strikeprice; T: the tenor; r:the drift=the risk-free rate; sigma:the volatility; N:the steps of the simulation
            //I: the number of the simulations
            double[] simulatedprice = new double[4];//output 4 different values.
            double t = T / N;
            //t is the time steps.
            //T is the time to maturity, the tenor.
            double disc = Math.Exp(-r * t);
            //disc is the discount value.
            double[,] V = new double[I , N ];
            //establish the stock price matrix.
            double[] Call = new double[I ];
            //this is the call matrix.
            double[] Put = new double[I ];
            //this is the put matrix.
            double[,] Average = new double[I , 2];
            double SumCall = 0;
            double SumPut = 0;
            double AveCall = 0;
            double AvePut = 0;
            
            for (uint i = 0; i<I; i++)
            {
                //establish the original matrix and give it number.
                V[i, 0] = S;            }
            for (uint i = 0; i<I; i++)
            {
                //establish random numbers
                for (uint j = 1; j<N ; j++)
                {
                    V[i, j] = V[i, j - 1] * Math.Exp((r - 0.5 * Math.Pow(sigma, 2)) * t + sigma * Math.Pow(t, 0.5) * this.randomNumbers[i,j]);
                }
                //this is used to calculate the average number of the European Option Price.
                    Call[i] = (Math.Max(V[i, N-1] - K, 0)) * disc;
                    SumCall = SumCall + Call[i];
                    Put[i] = (Math.Max(K - V[i, N-1], 0)) * disc;
                    SumPut = SumPut + Put[i];
            }
            
            for (uint i = 0; i<I; i++)
            {
                //this is used to calculate the standard error
                Average[i, 0] = Math.Pow((Call[i] - (SumCall / I)),2);
                Average[i, 1] = Math.Pow((Put[i] - (SumPut / I)),2);
                AveCall = AveCall + Average[i, 0];
                AvePut = AvePut + Average[i, 1];
            }
            AveCall = AveCall / (I - 1);
            AvePut = AvePut / (I - 1);
            simulatedprice[0] = SumCall / I;
            simulatedprice[1] = SumPut / I;
            simulatedprice[2] = Math.Sqrt(AveCall)/Math.Sqrt(I);
            simulatedprice[3] = Math.Sqrt(AvePut)/Math.Sqrt(I);
            return simulatedprice;
    }


   
        public double[] Greeks(double S, double K, double T, double r, double sigma, Int32 N, Int32 I )
        {
            double[] _Greeks= new double[10];
            double df = 0.001;
            _Greeks[0] = (SimulatedPrice(S + df * S, K, T, r, sigma, N, I)[0] - SimulatedPrice(S - df * S, K, T, r, sigma, N, I)[0]) / (2 * df * S);
            //the delta greek of European Call Option
            _Greeks[1] = (SimulatedPrice(S + df * S, K, T, r, sigma, N, I)[1] - SimulatedPrice(S - df * S, K, T, r, sigma, N, I)[1]) / (2 * df * S);
            //the delta greek of European Put Option.
            _Greeks[2] = (SimulatedPrice(S + S * df, K, T, r, sigma, N, I)[0] - 2 * SimulatedPrice(S, K, T, r, sigma, N, I)[0] + SimulatedPrice(S - df * S, K, T, r, sigma, N, I)[0]) / (df * df * S * S);
            //the gamma greek of European Call Option.
            _Greeks[3] = (SimulatedPrice(S + S * df, K, T, r, sigma, N, I)[1] - 2 * SimulatedPrice(S, K, T, r, sigma, N, I)[1] + SimulatedPrice(S - df * S, K, T, r, sigma, N, I)[1]) / (df * df * S * S);
            //the gamma greek of European Put Option.
            _Greeks[4] = (SimulatedPrice(S, K, T, r, sigma + df * sigma, N, I)[0] - SimulatedPrice(S, K, T, r, sigma - df * sigma, N, I)[0]) / (2 * df * sigma);
            //the vega greek of European Call Option
            _Greeks[5] = (SimulatedPrice(S, K, T, r, sigma + df * sigma, N, I)[1] - SimulatedPrice(S, K, T, r, sigma - df * sigma, N, I)[1]) / (2 * df * sigma);
            //the vega greek of European Put Option.
            _Greeks[6] = -((SimulatedPrice(S, K, T + df * T, r, sigma, N, I)[0] - SimulatedPrice(S, K, T, r, sigma, N, I)[0]) / (df * T));
            //the theta greek of European Call Option
            _Greeks[7] = -((SimulatedPrice(S, K, T + df * T, r, sigma, N, I)[1] - SimulatedPrice(S, K, T, r, sigma, N, I)[1]) / (df * T));
            //the theta greek of European Put Option.
            _Greeks[8] = (SimulatedPrice(S, K, T, r + r * df, sigma, N, I)[0] - SimulatedPrice(S, K, T, r - r * df, sigma, N, I)[0]) / (2 * df * r);
            //the rho greek of European Call Option
            _Greeks[9] = (SimulatedPrice(S, K, T, r + r * df, sigma, N, I)[1] - SimulatedPrice(S, K, T, r - r * df, sigma, N, I)[1]) / (2 * df * r);
            //the rho greek of European Put Option.
            return _Greeks;
        }

        
    }
    public class GaussianRNG
    {
        ///it is used to generate normally distributed random numbers
        ///Gaussian(Box-Muller) Random Number Generator Class
         double iset, gset;
         Random r1, r2;
        public GaussianRNG()
        {
            r1 = new Random(unchecked((int)DateTime.Now.Ticks));
            r2 = new Random(~unchecked((int)DateTime.Now.Ticks));
            iset = 0;
        }
        public double NextDouble()
        {
            double x, y, v1, v2;
            if (iset == 0)
            {
                do
                {
                    v1 = 2.0 * r1.NextDouble() - 1.0;
                    v2 = 2.0 * r2.NextDouble() - 1.0;
                    y = v1 * v1 + v2 * v2;
                }
                while (y >= 1.0 || y == 0.0);
                x = Math.Sqrt(-2.0 * Math.Log(y) / y);
                gset = v1 * x;
                iset = 1;
                return v2 * x;
            }
            else
            {
                iset = 0;
                return gset;
            }
        }
        public double[,] RandomMatrix(Int32 N, Int32 I)
        {
            double[,] RanNum = new double[I,N];
            for (uint i = 0 ; i < I ; i++)
            {
                for(uint j = 0 ; j < N ; j++)
                {
                    RanNum[i, j] = NextDouble();
                }
               
            }
            return RanNum;
        }
    }
    
}



